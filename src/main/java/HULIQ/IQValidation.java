package HULIQ;

import java.util.Calendar;

import org.apache.spark.SparkConf;
import org.apache.spark.api.java.JavaSparkContext;
import org.apache.spark.sql.DataFrame;
import org.apache.spark.sql.SQLContext;

public class IQValidation {

	/**
	 * @param args
	 */
	public static void main(String[] args) {
		// TODO Auto-generated method stub

		SparkConf conf = new SparkConf().setAppName("NormsCalculation");
		JavaSparkContext sc = new JavaSparkContext(conf);
		SQLContext sqlContext = new org.apache.spark.sql.SQLContext(sc);

		DataFrame iqInput = sqlContext
				.read()
				.format("com.databricks.spark.csv")
				.option("header", "true")
				.load("/home/minjar41/Downloads/HUL/HUL-IQ-test/IQ_Recos_Files/IQ_Recommendations/iQ_TmBkIpraveenkolge201509161458057055.Csv");
		DataFrame tmMaster = sqlContext
				.read()
				.format("com.databricks.spark.csv")
				.option("header", "true")
				.load("/home/minjar41/Downloads/HUL/sample-data/IQ_MasterTbl_Sampls.csv");
		// included for check basepack

		DataFrame productMaster = sqlContext
				.read()
				.format("com.databricks.spark.csv")
				.option("header", "true")
				.load("/home/minjar41/Downloads/HUL/Sample Data/input/reinvitationhuliqnormspocweeklystatuscheckweekl/Product Maste.csv");

		DataFrame delisted = sqlContext
				.read()
				.format("com.databricks.spark.csv")
				.option("header", "true")
				.load("/home/minjar41/Downloads/HUL/IQ_SampleData/DelistedCheck/DelistedSkusforMOC915PK.csv");

		DataFrame areaIQ = sqlContext.read().format("com.databricks.spark.csv")
				.option("header", "true")
				.load("/home/minjar41/Downloads/HUL/sample-data/AreaIQ.csv");

		// basic validations
		// Duplicate checks
		// DataFrame uniqueRows = iqInput.dropDuplicates();
		// valid and active hul_code
		// DataFrame validHul = iqInput.join(tmMaster,
		// iqInput.col("HUL_Code").equalTo(tmMaster.col("HUL_Code")),
		// "inner").where("Outlet_Flag = 1");

		// valid basepack
		// DataFrame validBasepack =
		// uniqueRows.join(productMaster,uniqueRows.col("Base_pack_Code").equalTo(productMaster.col("Basepack")),
		// "inner");

		// validation of Growth_type
		// DataFrame validWithType = iqInput
		// .where("Growth_Type = 'Width' OR Growth_Type = 'Widthio' OR Growth_Type = 'BM'");

		// date and year check
		// Calendar now = Calendar.getInstance();
		// Integer currentMonth = now.get(Calendar.MONTH) + 1;
		// Integer currentYear = now.get(Calendar.YEAR);
		// DataFrame validMocAndYear = iqInput.where("Year = '" + currentYear
		// + "' AND Moc = '" + currentMonth + "'");

		// Delisted check
		DataFrame areaDelistedJoin = delisted.join(
				areaIQ,
				delisted.col("New Branch /Area Name").equalTo(
						areaIQ.col("Area_Desc")), "inner").select(
				"Basepack Code", "Area_Desc", "Area");

		DataFrame outletAreaDelJoin = tmMaster.join(areaDelistedJoin,
				tmMaster.col("Area").equalTo(areaDelistedJoin.col("Area")),
				"inner").select(tmMaster.col("HUL_Code").alias("hc"),
				areaDelistedJoin.col("Basepack Code"));

		// areaDelistedJoin.show();
		System.out.println("\n\n\n\n\n");
		DataFrame delistedValidation = iqInput.join(
				outletAreaDelJoin,
				iqInput.col("HUL_Code")
						.equalTo(outletAreaDelJoin.col("hc"))
						.and(iqInput.col("Base_pack_Code").equalTo(
								outletAreaDelJoin.col("Basepack Code"))),
				"left_outer");
		DataFrame delistedValidationfinal = delistedValidation.where(
				delistedValidation.col("hc").isNull()
						.and(delistedValidation.col("Basepack Code").isNull()))
				.select("Year", "Moc", "RS_Code", "DB_Division", "HUL_Code",
						"Category", "PC", "Growth_Type", "Base_pack_Code",
						"Target_Value", "Target_Quantity", "Priority_Order",
						"Pack_Type", "Data_Source", "Data_Source_MOC",
						"Source_Details", "Slno", "Remarks");
		 

		long a = delistedValidation.count();
		long b = delistedValidationfinal.count();
		long c = outletAreaDelJoin.count();
		// long c = validHul.count();
		delistedValidationfinal.show();

		System.out.println(a + " this is with left_outer join \n\n\n\n\n");
		System.out.println(c + " this is with outer wala\n\n\n\n\n");
		System.out.println(b
				+ " this is with left_outer join with null\n\n\n\n\n");

		// System.out.println("\n\n\n\n" + areaDelistedJoin.count()
		// + "\n\n\n\n Iq input :" + iqInput.count()
		// + "\n\n\n\n\n   dilisted" + delistedValidation.count());
		// System.out.println(a + " this is dup");
		// System.out.println(b + " this is unique");
		// validHul.show();
		// System.out.println(c + " this is valid Year and Moc code");

		System.out.println("\n\n\n\n");

	}
}
